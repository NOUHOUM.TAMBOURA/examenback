import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EtudiantService} from "../../services/etudiant.service";
import {Etudiant} from "../../model/Etudiant";
import {Classe} from "../../model/Classe";

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.css']
})
export class ModifierComponent implements OnInit {

  idEtudiant?: number;
  nom?: string;
  prenom?: string;
  classeName?: string;
  listClasse: Classe[] = [];
  classeId?: any;
  etudiant?: Etudiant;
  classe?: Classe;
  selectedValue?: any;
  matricule?: string;


  constructor(private activeRoute: ActivatedRoute, private etudiantService: EtudiantService, private router: Router) {
    this.idEtudiant = activeRoute.snapshot.params['idEtudiant'];
  }

  ngOnInit(): void {
    this.getEtudiantById();
    this.getAllClasse();
  }

  getEtudiantById() {
    this.etudiantService.getEtudiantById(this.idEtudiant).subscribe((etudiant) => {
      this.nom = etudiant.nom;
      this.prenom = etudiant.prenom;
      this.classeName = etudiant.classe.nom;
      this.classeId = etudiant.classe.idClasse;
      this.matricule = etudiant.matricule;
      this.etudiant = etudiant;
    })
  }

  getAllClasse() {
    this.etudiantService.getAllClasse().subscribe((classes) => {
      this.listClasse = classes;
      console.log(this.listClasse)
    })
  }


  getAllEtudiant() {
    this.etudiantService.getAllEtudiant().subscribe((etudiant) => {
    })
  }


  onUpdate() {
    // @ts-ignore
    console.log(this.classeId, "ooooo")
    console.log(this.selectedValue, "yyyyyy")
    if (!this.selectedValue) {
      this.selectedValue = this.classeId;
    }
    console.log(this.selectedValue)
    this.etudiantService.getClasseById(this.selectedValue).subscribe((classe) => {
      this.classe = classe;
      let etudiantOjet: any = {
        "matricule": this.matricule,
        "nom": this.nom,
        "prenom": this.prenom,
        "classe": this.classe
      }
      console.log(etudiantOjet)
      this.etudiantService.updateEtudiant(etudiantOjet, this.idEtudiant).subscribe((etudiantOjet) => {
      this.onAluner();
      this.getAllEtudiant();
      });
    })
  }

  onAluner() {
    this.router.navigateByUrl("/etudiant");
  }
}
