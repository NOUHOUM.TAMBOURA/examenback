import {Component, OnInit} from '@angular/core';
import {EtudiantService} from "../../services/etudiant.service";
import {Classe} from "../../model/Classe";
import {Etudiant} from "../../model/Etudiant";
import {Router} from "@angular/router";

@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.css']
})
export class EtudiantComponent implements OnInit {

  filterTerm!: string;

  listClasse: Classe[] = [];
  addEtudiantObject: any;
  classe?: Classe;
  etudiants!: Etudiant[];

  nom?: string;
  prenom?: string;

  p!: number;

  constructor(private etudiantService: EtudiantService, private route: Router) {

  }

  ngOnInit(): void {
    this.getAllClasse();
    this.getAllEtudiant();
  }

  getAllClasse() {
    this.etudiantService.getAllClasse().subscribe((classes) => {
      this.listClasse = classes;
    })
  }

  getAllEtudiant() {
    this.etudiantService.getAllEtudiant().subscribe((etudiant) => {
      this.etudiants = etudiant;
    })
  }

  onSubmit(etudiantForm: { value: any; }) {
    this.addEtudiantObject = etudiantForm.value;
    this.etudiantService.getClasseById(this.addEtudiantObject.classe).subscribe((classe) => {
      this.classe = classe;
      let etudiantOjet: Etudiant = {
        "nom": this.addEtudiantObject.nom,
        "prenom": this.addEtudiantObject.prenom,
        "classe": this.classe
      }
      this.etudiantService.addEtudiant(etudiantOjet).subscribe((etudiantOjet) => {
        // @ts-ignore
        this.etudiants?.push(etudiantOjet);
        this.nom = "";
        this.prenom = "";
        this.getAllEtudiant();
      });

    })
  }

  onDelete(id: Etudiant) {
    let v = confirm("test");
    if(v)
    this.etudiantService.deleteById(id.idEtudiant).subscribe(() => {
      this.getAllEtudiant();
    });
  }

  onUpdate(et: Etudiant) {
    let id = et.idEtudiant;
    this.route.navigateByUrl("/editEtudiant/"+ id);
  }
}
