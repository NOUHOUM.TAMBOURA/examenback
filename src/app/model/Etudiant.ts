import {Classe} from "./Classe";

export interface Etudiant {
  matricule?: string;
  idEtudiant?: number;
  nom: string;
  prenom: string;
  classe: Classe;

}
