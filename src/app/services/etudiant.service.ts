import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Classe} from "../model/Classe";
import {environment} from "../../environments/environment";
import {Etudiant} from "../model/Etudiant";


@Injectable({
  providedIn: 'root'
})
export class EtudiantService {

  constructor(private http: HttpClient) { }

  getAllClasse() {
    return this.http.get<Classe[]>(environment.baseUrl + "/listClasse");
  }

  getAllEtudiant() {
    return this.http.get<Etudiant[]>(environment.baseUrl + "/listEtudiant");
  }

  getClasseById(idClasse: number) {
    return this.http.get<Classe>(environment.baseUrl + "/listByIdClasse/" + idClasse);
  }

  addEtudiant(etudiant: Etudiant) {
    return this.http.post<Etudiant[]>(environment.baseUrl + "/addEtudiant/", etudiant);
  }

  updateEtudiant(etudiant: Etudiant, idEtudiant: number | undefined) {
    return this.http.post<Etudiant>(environment.baseUrl + "/updateEtudiant/"+idEtudiant, etudiant);
  }

  deleteById(idEtudiant: number | undefined) {
    return this.http.delete<Etudiant[]>(environment.baseUrl + "/deleteById/" + idEtudiant);
  }

  getEtudiantById(idEtudiant: number | undefined) {
    return this.http.get<Etudiant>(environment.baseUrl + "/listByIdEtudiant/" + idEtudiant);
  }
}
