import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EtudiantComponent} from "./component/etudiant/etudiant.component";
import {ModifierComponent} from "./component/modifier/modifier.component";
import {MenuComponent} from "./component/menu/menu.component";

const routes: Routes = [
  { path: "", redirectTo: "etudiant", pathMatch: "full" },
  {path: "etudiant", component : EtudiantComponent},
  {path: "editEtudiant/:idEtudiant", component : ModifierComponent},
  {path: "menu", component : MenuComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
